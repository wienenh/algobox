//
//  AppDelegate.h
//  algobox
//
//  Created by Hans Wienen on 02/04/2020.
//  Copyright © 2020 Hans Wienen. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

