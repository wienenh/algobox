#  Notes on versions

During development (after finishing week 3 of the Algorithms IV course by Tim Roughgarden) I started experimenting with threading and updating
UI elements during the calculation of the answer to some NP-complete problem (Travelling Salesman). I inadvertently submitted the half-backed
experiment to the master branch. So I split off the original solution (by deleting the threading stuff) to the branch `back-to-unthreaded-solution`, revisited the main branch,
and split that off to `starting-threaded-solution-using C++`. I then got the code in `back-to-unthreaded-solution` working and overwrote the master branch.

