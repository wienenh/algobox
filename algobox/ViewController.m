//
//  ViewController.m
//  algobox
//
//  Created by Hans Wienen on 02/04/2020.
//  Copyright © 2020 Hans Wienen. All rights reserved.
//

#import "ViewController.h"
#include "algorithms/TravellingSalesman.h"
#include <assert.h>
#include <pthread.h>

@implementation ViewController

static void * TravellingSalesmanCurrentProgressContext = &TravellingSalesmanCurrentProgressContext;
static void * TravellingSalesmanCurrentCostsContext = &TravellingSalesmanCurrentCostsContext;


- (void)viewDidLoad {
    [super viewDidLoad];

    [self.progressBar setCanDrawConcurrently:YES];
    [self.progressBar setUsesThreadedAnimation:YES];
    [self.progressBar setMinValue:0];
    [self.progressBar setMaxValue:1];
    [self.progressBar setDoubleValue:0.0];

    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (IBAction)openFileButton:(id)sender {
    
    NSOpenPanel* openDialog = [NSOpenPanel openPanel];
    [openDialog setCanChooseFiles:YES];
    [openDialog setCanChooseDirectories:YES];
    [openDialog setAllowsMultipleSelection:NO];
    if ( [openDialog runModal] == NSModalResponseOK ) {
        NSArray *urls = [openDialog URLs];
        NSURL *url = [urls objectAtIndex:0];
        [self.fileNameTextField setStringValue:[url lastPathComponent]];
        
        TravellingSalesman * t = [[TravellingSalesman alloc] init];
        [t loadFile: [url path]];
        
        // Register this view controller as an observer
        [t addObserver:self forKeyPath:@"currentProgress" options:NSKeyValueObservingOptionNew context:TravellingSalesmanCurrentProgressContext];
        [t addObserver:self forKeyPath:@"currentCosts" options:NSKeyValueObservingOptionNew context:TravellingSalesmanCurrentCostsContext];

        // Execute the main part of the algorithm off the main thread to keep the UI responsive
        NSThread * thread = [[NSThread alloc] initWithTarget:t selector:@selector(getHeuristicTour) object:nil];
        [thread start];
    }
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    // If the currentCosts changes, update the text field
    // If the currentProgress changes, update the progress bar
    
    NSValue * v = [change objectForKey:NSKeyValueChangeNewKey];
    SEL selector = nil;
    
    if (context == TravellingSalesmanCurrentCostsContext) {
        selector = @selector(updateTextField:);
    } else if (context == TravellingSalesmanCurrentProgressContext) {
        selector = @selector(updateProgressBar:);
    }
    
    // Run the UI update on the main thread
    [self performSelectorOnMainThread:selector withObject:v waitUntilDone:YES];
}

- (void) updateProgressBar: (NSValue *) update {
    
    const char * type = [update objCType];
    
    if (0 == strcmp(type, @encode(double))) {
        double d;
        [update getValue:&d];
        [self.progressBar setDoubleValue:d];
    } else {
        NSLog(@"Encountered unhandled NSValue with encoding %s", type);
    }
}

- (void) updateTextField: (NSObject *) update {
    if ([update isKindOfClass:[NSValue class]]) {
        NSValue *o = (NSValue *) update;
        const char * type = [o objCType];
        if (0 == strcmp(type, @encode(double))) {
            double d;
            [o getValue:&d];
            [[self progressField] setDoubleValue:d];
        } else {
            NSLog(@"Encountered unhandled NSValue with encoding %s", type);
        }
    } else if ([update isKindOfClass:[NSString class]]) {
        NSString * s = (NSString *) update;
        [[self progressField] setStringValue:s];
    } else {
        NSLog(@"Encountered unrecognized object in ViewController:updateTextField:");
    }
    

}

@end
