//
//  TravellingSalesman.h
//  algobox
//
//  Created by Hans Wienen on 05/04/2020.
//  Copyright © 2020 Hans Wienen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"


struct Location {
    double x;
    double y;
};

@interface TravellingSalesman : NSObject
{
    int numberOfCities;
    Location * cities;
    ViewController * delegate;
}

@property double currentProgress;
@property double currentCosts;

- (BOOL) loadFile:(NSString *) fileName;
- (double) getDistanceI:(int) i j:(int) j;
- (double) getHeuristicTour;
- (void) setDelegate:(NSObject *) delegate;

@end

