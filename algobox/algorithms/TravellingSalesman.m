//
//  TravellingSalesman.m
//  algobox
//
//  Created by Hans Wienen on 05/04/2020.
//  Copyright © 2020 Hans Wienen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TravellingSalesman.h"
#include "stdio.h"
#include "math.h"

@implementation TravellingSalesman

@synthesize currentProgress;
@synthesize currentCosts;

- (void) setDelegate:(ViewController *) delegate {
    self->delegate = delegate;
}

- (BOOL) loadFile:(NSString *)fileName {
    FILE *fp = fopen([fileName fileSystemRepresentation], "r");
    if (!fp) {
        NSLog(@"Could not open %s", [fileName fileSystemRepresentation]);
        return NO;
    }
    
    fscanf(fp, "%i\n", &numberOfCities);
    
    self->cities = new Location[numberOfCities];

    int city;
    double x, y;
    while (!feof(fp)) {
        fscanf(fp, "%i %lf %lf\n", &city, &x, &y);
        Location l = {x, y};
        self->cities[city-1] = l;
    }
    
    return YES;
}

- (double) getDistanceI:(int)i j:(int)j {
    Location l = cities[i];
    Location m = cities[j];
    double d = sqrt((l.x - m.x)*(l.x - m.x) + (l.y - m.y)*(l.y - m.y));

    return d;
}
 
- (double) getHeuristicTour {
    BOOL visited[numberOfCities];
    
    for (int i = 0; i < numberOfCities; i++) visited[i] = NO;
    visited[0] = YES;
    
    int city=0;
    
    for (int i = 1; i < numberOfCities; i++) {
        self.currentProgress = ((double)i)/numberOfCities;
        double oneLeg = INFINITY;
        int candidate = -1;
        for (int j = 0; j < numberOfCities; j++) {
            double candidate_distance = [self getDistanceI:city j:j];
            if (candidate_distance < oneLeg && !visited[j] && candidate_distance > 0) {
                oneLeg = candidate_distance;
                candidate = j;
            }
        }
        
        self.currentCosts += oneLeg;
        city = candidate;
        visited[candidate] = YES;
    }
    
    self.currentCosts += [self getDistanceI:city j:0];
    self.currentProgress = 1.0;
    
    return [self currentCosts];
}

@end
