//
//  algoboxTests.m
//  algoboxTests
//
//  Created by Hans Wienen on 02/04/2020.
//  Copyright © 2020 Hans Wienen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UpdateWrapper.h"

@interface algoboxTests : XCTestCase


@end

@implementation algoboxTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}


- (void) testUpdateWrapper {
    double epsilon = 1e02;

    UpdateWrapper * uw = [[UpdateWrapper alloc] initWithDouble: 2.5];
    XCTAssertEqualWithAccuracy([uw doubleValue], 2.5, epsilon);
    XCTAssertEqual([uw updateType], uDouble);
    
    [uw setStringValue:@"This is an update message"];
    XCTAssertEqual([uw stringValue], @"This is an update message");
    XCTAssertEqual([uw updateType], uString);
    
    [uw setDoubleValue:3.5];
    XCTAssertEqualWithAccuracy([uw doubleValue], 3.5, epsilon);
    XCTAssertEqual([uw updateType], uDouble);
}


@end
